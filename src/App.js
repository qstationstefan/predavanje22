import CSSModules from "./cssmodules/avatarModules/CSSModules";
import StyledButtonModule from "./cssmodules/styledButtonModule/StyledButtonModule";
import StyledButton from "./styledcomponents/StyledButton";


function App() {
  return (
    <StyledButtonModule />
    // <CSSModules />
    // <StyledButton />
  );
}

export default App;
