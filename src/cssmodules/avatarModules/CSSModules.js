import styles from "./styles.module.css";

const isValid = true;
const isRed = true;
const CSSModules = () => {
  return (
    <section className={styles.testimonial}>
      <div className={`${isValid && styles["testimonial-wrapper"]} ${isRed ? styles["red"] : styles["black"]}`}>
        <img
          src="https://randomuser.me/api/portraits/women/48.jpg"
          alt="Tammy Stevens"
          className={styles["testimonial-avatar"]}
        />
        <div>
          <p className={styles["testimonial-quote"]}>
            This is one of the best developer blogs on the planet! I read it
            daily to improve my skills.
          </p>
        </div>
        <p className={`${true && styles["testimonial-name"]}  ${false && styles["invalid"]}`}>
          Tammy Stevens
          <span> · Front End Developer</span>
        </p>
      </div>
    </section>
  );
};

export default CSSModules;
