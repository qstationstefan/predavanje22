import styles from "./StyledButtonModule.module.css"

const isValid = false;
const StyledButtonModule = () => {
  return (
    <div>
      <button className={`${!isValid && styles['invalid']}`}>Click me</button>
    </div>
  );
}

export default StyledButtonModule;